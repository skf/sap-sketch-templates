## Intro
These downloadable sketch files contain SAP Fiori for iOS UI compoments for compact width as well as regular width. UI elements like bars, controls, views and some examples of patterns and frameworks are also included. 

## Before you get started
Download the San Francisco fonts [here](https://developer.apple.com/fonts/).

## System Requirements
The SKF Sketch Design Library requires a computer running OS X with Sketch installed. 
